
# Llandudno

This repository contains the [Ardour](https://ardour.org/) digital audio workstation project files for the track [llandudno](https://theargentgrub.bandcamp.com/track/llandudno) by [The Argent Grub](https://theargentgrub.co.uk/).

## Installation
You wil need a working copy of [Ardour](https://ardour.org/) installed on your system. Ardour is available for most platforms, but this was created on Linux.

You will need Git

You will also need [Git Large File Storage (LFS)]( https://git-lfs.com/) in order to handel the wav files.

Clone this repository to your local machine.

Open Ardour and select this project to work.


### What might not work?

#### Plugins
Any plugins used in the project will need to be installed locally as well.

You should get a warning if Ardour cannot find them, and you should then be able to track them down.

Plugins in use are:
* LV2    sfizz (by SFZTools)
* LV2    Invada Stereo Phaser (stereo in) (by Invada)
* LV2    GxAutoWah (by Guitarix team)
* LV2    Dragonfly Room Reverb (by Michael Willis)
* LV2    GxEcho-Stereo (by Guitarix team)
* LV2    ACE EQ (by Ardour Community)
* LV2    Calf Equalizer 12 Band (by Calf Studio Gear)
* LV2    kpp_octaver (by Oleg Kapitonov)
* LADSPA TAP AutoPanner (by Tom Szilagyi)
* LV2    Calf Vintage Delay (by Calf Studio Gear)
* LV2    Dragonfly Hall Reverb (by Michael Willis and Rob vd Berg)


## Usage
Fork this project or ask to join the group.

Create a branch to make your modifications.

## Contributing

Use pull requests to share your contributions.

## License
The content here is licensed under a Creative Commons Licence by [The Argent Grub] under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0?ref=chooser-v1) 


# Credits
Using GitLab to version control music making was inspired by creative commons band [Lorenzo's Music](https://www.lorenzosmusic.com/?utm_source=github&utm_medium=bandlink&utm_campaign=withyoureadme) and their post [Brainstorming Using Github For Music With Ubuntu Studio And Ardour To Record Remotely.](https://www.lorenzosmusic.com/2020/11/brainstorming-using-github-for-music.html?utm_source=github&utm_medium=articlelink&utm_campaign=withyoureadme)